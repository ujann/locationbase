package org.svfhain.locbase.controller;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.svfhain.locbase.model.Person;
import org.svfhain.locbase.repos.PersonRepository;

@Controller @RequestMapping("/api/person")
public class PersonController {
    static final Logger logger = LoggerFactory.getLogger(PersonController.class);

	@Inject
	private PersonRepository personRepository;

	@RequestMapping(method = RequestMethod.GET ) 
	public @ResponseBody List<Person> getAllPersons(){
		return personRepository.findAll();
	}
	
	@RequestMapping(value = "{id}",method = RequestMethod.GET ) 
	public @ResponseBody Person getById(@PathVariable( "id" ) final String id){
		return personRepository.findById(id);
	}
	
	@RequestMapping(method = RequestMethod.POST ) @ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody Person create( @RequestBody Person person){
		return personRepository.save(person);
	}
	
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public void update(@RequestBody Person person, @PathVariable String id) {
        Assert.isTrue(person.getId().equals(id));
        personRepository.update(person);
    }

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE )
	public @ResponseBody void deletePerson(@PathVariable( "id" ) final String id){
		personRepository.delete(id);
	}
	
	
}

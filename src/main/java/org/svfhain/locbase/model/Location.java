package org.svfhain.locbase.model;

import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;

/**
 * 
 * represents a location on earth by longitude and latitude - springdata and mongo need these values as long[]
 * so we hide these 'internals'
 *
 */
public class Location {
	@GeoSpatialIndexed
	private double[] geoLocation = new double[2];
	
	public void setLongitude(double longitude){
		this.geoLocation[0] = longitude;
	}
	public double getLongitude(){
		return this.geoLocation[0];
	}

	public void setLatitude(double longitude){
		this.geoLocation[1] = longitude;
	}
	public double getLatitude(){
		return this.geoLocation[1];
	}
}

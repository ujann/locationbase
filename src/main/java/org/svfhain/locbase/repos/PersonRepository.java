package org.svfhain.locbase.repos;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import org.svfhain.locbase.model.Person;

@Repository
public class PersonRepository {
	@Inject
	private MongoTemplate mongoTemplate;
	
	public Person findById(String id){
		return mongoTemplate.findById(id, Person.class);
	}

	public Person save(Person person) {
		mongoTemplate.save(person);
		return person;
	}

	public List<Person> findAll() {
		return mongoTemplate.findAll(Person.class);
	}

	public void delete(String id) {
		Person person = new Person();
		person.setId(id);
		mongoTemplate.remove(person);
	}

	public void update(Person person) {
		mongoTemplate.save(person);
	}

}

package org.svfhain.locbase.web;

import java.net.UnknownHostException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.svfhain.locbase.controller.PersonController;
import org.svfhain.locbase.repos.PersonRepository;

import com.mongodb.Mongo;


@Configuration @EnableWebMvc
// TODO: die angabe von "basePackage" hat nicht funktioniert, daher die angabe von 2 klassen :-(
@ComponentScan(basePackageClasses = {PersonController.class,PersonRepository.class})//org.svfhain.locbase,
public class WebConfig extends WebMvcConfigurerAdapter {
    
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	registry.addResourceHandler("**").addResourceLocations("/");
    }

    @Bean
    public Mongo mongo() throws UnknownHostException {
        return new Mongo("localhost");
    }
    
    @Bean
    public MongoTemplate mongoTemplate() throws UnknownHostException {
        return new MongoTemplate(mongo(), "loc");
    }
       
}

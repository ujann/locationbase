'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/view1', {template: 'partials/partial1.html', controller: MyCtrl1});
    $routeProvider.when('/view2', {template: 'partials/partial2.html', controller: MyCtrl2});
    $routeProvider.when('/view3', {template: 'partials/partial3.html', controller: AboutMeController});
    $routeProvider.when('/maps', {template: 'partials/maps.html', controller: MyCtrl1});
    $routeProvider.when('/filters', {template: 'partials/filters.html', controller: MyCtrl2});
    $routeProvider.when('/aboutMe', {template: 'partials/aboutMe.html', controller: AboutMeController});
    $routeProvider.when('/list', {template: 'partials/list.html', controller: AboutMeController});
    $routeProvider.when('/chat', {template: 'partials/chat.html', controller: AboutMeController});
    $routeProvider.otherwise({redirectTo: '/view3'});
  }]);

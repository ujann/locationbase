'use strict';
/* App Controllers */

function MyCtrl1() {}
MyCtrl1.$inject = [];


function MyCtrl2() {
}
MyCtrl2.$inject = [];

function AboutMeController($scope, dataService) {
	dataService.get({id:123}, function(person) {
		console.log(person);
		$scope.name = person.userName;
		$scope.person = person;
		$scope.categories = [ {
			id : 1,
			name : "Geocaching"
		}, {
			id : 2,
			name : "Scottland Yard"
		}, {
			id : 3,
			name : "Windsurfen"
		} ];
		$scope.activeCategoryId = 2;
		$scope.setPosition = function(){
			$scope.person.$save();
		};
		
	});
	//$scope.setPosition = setPosition;
}
AboutMeController.$inject = ['$scope', 'dataService'];

'use strict';

// Demonstrate how to register services
// In this case it is a simple constant service.
var servicesModule = angular.module('myApp.services', []);
servicesModule.value('version', '0.1');

servicesModule.factory('dataService', [ '$resource', function($resource) {
	return $resource('/person/:id', {
		id : '@id'
	});
} ]);

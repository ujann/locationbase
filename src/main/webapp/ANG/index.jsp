<html>
<head>

<style type="text/css">
.stdBorder {
	margin: 2px;
	border-width: 2px;
	border-style: solid;
	border-color: blue;
	padding: 5px;
}

#header {
	widht: 100%;
	height: 50px;
}

#leftcol {
	float: left;
}

#rightcol {
	float: right;
}

#aboutme {
	float: left;
	width: 300px;
	height: 250px;
}

#filters {
	float: right;
	width: 300px;
	height: 250px;
}

#listview,#chat {
	width: 300px;
	height: 250px;
}

#maps {
	height: 350px;
}

.label { width: 100px;}

.clearfix {
	clear: both;
}
</style>
<script src="angular.js" ng:autobind></script>

</head>
<body>

	<div id="main">

		<div id="header" class="stdBorder">
			<h1>LOCATION BASE</h1>
		</div>
		<div id="leftcol" class="stdBorder" style="border-color: green;">
			Leftcol
			<div id="firstline" class="stdBorder" style="border-color: yellow;">
				<div id="aboutme" class="stdBorder">
					<h2>About me:</h2>
					<div><span class="label">Name:</span><span>Volker</span></div>
					<div><span class="label">Position:</span><input type="text" value="12.45"></input><input type="text" value="13.11"></input></div>
					<div><span class="label">Categories:</span><select multiple="multiple"><option>Geocaching</option><option>Schotland Yard</option></select></div>
				</div>
				<div id="filters" class="stdBorder">
					<h2>filters</h2>
				</div>
				<div class="clearfix"></div>
			</div>
			<div id="maps" class="stdBorder" style="border-color: orange;">MAPS</div>
		</div>
		<div id="rightcol" class="stdBorder" style="border-color: red;">
			Rightcol
			<div id="listview" class="stdBorder">LIST</div>
			<div id="chat" class="stdBorder">CHAT</div>
		</div>
		<div class="clearfix"></div>
	</div>

</body>
</html>

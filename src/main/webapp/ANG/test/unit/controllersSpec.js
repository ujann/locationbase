/* jasmine specs for controllers go here */

describe('AboutMeController', function() {
	beforeEach(module('myApp.services'));

	var scope;
	var aboutMeController;

	beforeEach(inject(function($rootScope, $controller, dataService) {
		scope = $rootScope.$new();

		// mock the data service
		dataService.get = jasmine.createSpy("dataService spy").andCallFake(
				function() {
					scope.name = "vollesommi";
				});

		// TODO: is this better than mocking the data service?
		// $browser = scope.$service('$browser');
		// 
		// $browser.xhr.expectGET('/person/123').respond([{name: 'Nexus S'},
		// {name: 'Motorola DROID'}]);

		aboutMeController = $controller(AboutMeController, {
			$scope : scope,
			dataService : dataService
		});
		expect(dataService.get).toHaveBeenCalled();

	}));

	it('should set the name in the scope to vollesommi', function() {
		expect(scope.name).toBe('vollesommi');
	});
});
/* jasmine specs for services go here */

describe('service', function() {
	beforeEach(module('myApp.services'));

	describe('version', function() {
		it('should return current version', inject(function(version) {
			expect(version).toEqual('0.1');
		}));
	});
	describe('injection of rest data service', function() {
		it('should return new rest resource', inject(function(dataService) {
			expect(dataService).toBeDefined();
		}));
	}); 
});

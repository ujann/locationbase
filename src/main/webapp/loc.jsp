<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Locations</title>
<link href="loc.css" media="all" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='steal/steal.js?locationbase'></script>

<script src="lib/json2.js"></script>
<script src="lib/jquery-1.7.1.js"></script>
<script src="lib/underscore-1.1.6.js"></script>
<script src="lib/backbone-0.5.3.js"></script>
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?v=3.7&sensor=true&key=AIzaSyCCsRYCih-LVaIT_ZwYQ54KI2HYJ2qNb_U">
	
</script>
<script type="text/javascript">
	var LOC = LOC || {};

	function initGmap() {
		var options = {
			center : new google.maps.LatLng(52.51, 13.45),
			zoom : 15,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
		LOC.map = new google.maps.Map(document.getElementById("mapContainer"),
				options);
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = new google.maps.LatLng(position.coords.latitude,
						position.coords.longitude);
				new google.maps.InfoWindow({
					map : LOC.map,
					position : pos,
					content : 'Here are you!'
				});
				LOC.map.setCenter(pos);
			}, function() {
				alert("You dont allow me no geolocation!!");
			});
		} else {
			// Browser doesn't support Geolocation
			alert("Youve got no geolocation!!");
		}
		google.maps.event.addListener(LOC.map, 'bounds_changed', function() {
			var bounds = LOC.map.getBounds();
			if (bounds) {
				var southWest = bounds.getSouthWest();
				var northEast = bounds.getNorthEast();
				$('#longMin').html(southWest.lng());
				$('#longMax').html(northEast.lng());
				$('#latMin').html(southWest.lat());
				$('#latMax').html(northEast.lat());

			} else {
				alert("no bounds!");
			}
		});
		google.maps.event.addListener(LOC.map, 'click', function(event) {
			$('#location_latitude').val(event.latLng.lat());
			$('#location_longitude').val(event.latLng.lng());
		});

	}
	function fillUserList() {
		$.get("/loc/api/person", function(data) {
			$('#personTableBody').empty();
			LOC.users = [];
			$(data)
					.each(
							function(idx, elem) {
								var newRow = $('<tr/>');
								newRow.append($('<td/>').html(elem.userName));
								newRow.append($('<td/>').html(
										elem.location.longitude));
								newRow.append($('<td/>').html(
										elem.location.latitude));
								newRow.append($('<td/>').append(
										$('<button/>').attr('id',
												'del_' + elem.id).addClass(
												'delete').html('DEL')));
								$('#personTableBody').append(newRow);
								LOC.users.push(elem);
							});
			$('.delete').click(deletePerson);
			$(LOC.users).each(
					function(idx, person) {
						//alert("set a marker with '" + elem.userName + "' at " + elem.location.lat() + "," + elem.location.lng());
						var markerPosition = new google.maps.LatLng(
								person.location.latitude,
								person.location.longitude);

						var marker = new google.maps.Marker({
							position : markerPosition,
							map : LOC.map,
							title : person.userName,
							draggable : true
						});
						google.maps.event.addListener(marker, 'dragend',
								function() {
									console.log("Drag ended: Lat:"
											+ this.get('position').lat());
								});
					});
		});

	}
	function deletePerson(evt) {
		var idToDelete = evt.target.id.substr(4);
		$.ajax({
			url : "/loc/api/person/" + idToDelete,
			type : "DELETE",
			success : function(data) {
				fillUserList();
				$("input[type='text']").val('');
			},
		});
		return false;

	}
	
	// FIXME: just for testing:
	function testit() {
		Locationbase.Models.Person.findAll({}, function(persons) {
			//alert(persons);
			//console.log(persons);
		})
	}
	
	$(function() {
		testit();
		initGmap();
		fillUserList();
		$('#formSubmit').click(function() {
			var newPerson = {};
			newPerson.userName = $('#userName').val();
			newPerson.location = {};
			newPerson.location.longitude = $('#location_longitude').val();
			newPerson.location.latitude = $('#location_latitude').val();

			$.ajax({
				url : "/loc/api/person",
				type : "POST",
				data : JSON.stringify(newPerson),
				success : function(data) {
					fillUserList();
					$("input[type='text']").val('');
				},
				dataType : "json",
				contentType : "application/json"
			});
			return false;
		});
	});
</script>
</head>
<body>
	<h1>New</h1>
	<form>
		<input type="text" name="userName" id="userName" /> <input
			type="text" name="location_longitude" id="location_longitude" /> <input
			type="text" name="location_latitude" id="location_latitude" /> <input
			type="submit" id="formSubmit" value="Save" />
	</form>

	<h1>Persons</h1>
	<table id="personTable">
		<thead>
			<tr>
				<th>username</th>
				<th>long</th>
				<th>lat</th>
			</tr>
		</thead>
		<tbody id="personTableBody">
		</tbody>
	</table>
	<h1>Map:</h1>
	<div>
		Long. from <span id="longMin"></span> to <span id="longMax"></span>,
		Lat. from <span id="latMin"></span> to <span id="latMax"></span>
	</div>
	<div id="mapContainer" style="width: 1000px; height: 500px"></div>
</body>
</html>